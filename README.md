# Personal Website

## Description
This is my personal website. It is an HTML/CSS front-end utilizing a bootstrap framework, and a simple JavaScript back-end utilizing an nginx web server. All images are hosted on a public AWS S3 bucket and the website is hosted on an AWS linux EC2 instance. My personal developement is automated through a gitlab CI/CD pipeline.   

### Local Deployment (Docker)
For a more production-like environment, you can use Docker to deploy the website locally. This method uses nginx, similar to the production setup.

1. Install Docker:
   Follow the instructions at https://docs.docker.com/get-docker/ to install Docker on your machine.

2. Clone the repository:
   ```
   git clone https://gitlab.com/aklavo/personal-website.git
   cd personal-website
   ```

3. Build the Docker image:
   ```
   docker build -t my-website .
   ```

4. Run the Docker container:
   ```
   docker run -p 8080:80 my-website
   ```

5. Access the website:
   Open a web browser and navigate to `http://localhost:8080`

To stop the container, press Ctrl+C in the terminal where it's running.

### Manual EC2 Server Configuration
Below are the manual steps I did to setup my website intially.  

Connect to the EC2 instance. Then update all packages:
 ```
 sudo apt update && sudo apt upgrade -y
 ```
 Install the web server nginx:
 ```
sudo apt install nginx -y
 ```
 Enable and start nginx:
 ```
sudo systemctl start nginx
sudo systemctl enable nginx
```
Clone the website repo into the web server directory:
```
git clone https://gitlab.com/aklavo/personal-website.git /var/www/html
```
Change ownership of the website directory to nginx user and run nginx config test:
```
sudo chown -R www-data:www-data /var/www/html
sudo nginx -t
```
Lastly reload nginx:
```
sudo systemctl reload nginx
```

## Roadmap
1.1 Update and fix bugs
    - [ ] Fix all social links
    - [ ] Fix all stava embeded content
    - [ ] Update page content
    - [ ] Route note links to markdown files in repo some how
1.2 Get certificate
    - [ ] Get certificate
    - [ ] Automate updating certificate
1.3 Deploy Website Analytics
    - [ ] Track who's visiting and from where

## Authors and acknowledgment
Sole contributor: Andrew Klavekoske

## Project status
Actively Deployed - updated perodically
