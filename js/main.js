// Define client-side routes
const routes = {
  '/': { path: 'home', type: 'html' },
  '/resume': { path: 'mainPages/resume', type: 'html' },
  '/publications': { path: 'mainPages/publications', type: 'html' },
  '/references': { path: 'mainPages/references', type: 'html' },
  '/projects': { path: 'mainPages/projects', type: 'html' },
  '/courses': { path: 'mainPages/courses', type: 'html' },
  '/notes/linux': { path: 'notes/linux', type: 'html' },
  '/notes/linux/linux-terminal': { path: 'Linux-notes/linux-terminal', type: 'markdown' },
  '/notes/linux/linux-file-system': { path: 'Linux-notes/linux-file-system', type: 'markdown' },
  '/notes/linux/linux-task-automation': { path: 'Linux-notes/linux-task-automation', type: 'markdown' },
  '/notes/linux/linux-file-archiving-compression': { path: 'Linux-notes/linux-file-archiving-compression', type: 'markdown' },
  '/notes/git': { path: 'notes/git', type: 'html' },
  '/notes/git/git-basics': { path: 'Git-notes/git', type: 'markdown' },
  '/notes/bootstrap': { path: 'notes/bootstrap', type: 'html' },
  '/notes/training-plans': { path: 'notes/training', type: 'html' },
  '/notes/personal-records': { path: 'notes/records', type: 'html' },
  '/notes/resort-weather': { path: 'notes/resortWeather', type: 'html' },
  '/notes/rubiks-cube': { path: 'notes/rubiksCube', type: 'html' },
  '/404': { path: 'error/404', type: 'html' }
};

// Function to load HTML template into the content div within index.html
function loadTemplate(template) {
  if (!template) {
    console.error('Route not found:', template);
    return loadTemplate(routes['/404']);
  }

  const { path, type } = template;
  console.log(`Loading template: ${path}`);

  // Helper function to fetch and render content
  const fetchAndRender = (url, processor) => {
    return fetch(url)
      .then(response => {
        if (!response.ok) throw new Error('Network response was not ok');
        return response.text();
      })
      .then(processor)
      .catch(error => {
        console.error('Error loading template:', error);
        loadTemplate(routes['/404']);
      });
  };

  if (type === 'markdown') {
    const githubRepoUrl = `https://raw.githubusercontent.com/aklavo/notes/main/${path}.md`;
    fetchAndRender(githubRepoUrl, markdown => {
      if (typeof marked.parse !== 'function') {
        throw new Error('marked.parse is not a function');
      }
      document.getElementById('content').innerHTML = marked.parse(markdown);
    });
  } else {
    const localUrl = `/templates/${path}.html`;
    fetchAndRender(localUrl, html => {
      document.getElementById('content').innerHTML = html;
      initializeStravaEmbeds(); // Initialize Strava embeds after loading the template
    });
  }
}

// Function to handle route changes
function handleRoute(event) {
  let path;
  if (event) {
    event.preventDefault();
    const url = new URL(event.target.href);
    if (url.origin !== window.location.origin) {
      return window.open(event.target.href, '_blank');
    }
    path = url.pathname;
  } else {
    path = window.location.pathname;
  }

  console.log(`Handling route: ${path}`);
  const template = routes[path] || routes['/404'];
  loadTemplate(template);

  if (path !== '/404') {
    window.history.pushState({}, '', path);
  }
}

// Function to initialize Strava embeds
function initializeStravaEmbeds() {
  if (typeof StravaEmbed !== 'undefined') {
    console.log('Initializing Strava embeds');
    StravaEmbed.init();
  } else {
    console.error('StravaEmbed is not defined');
  }
}

// Function to load the Strava embed script dynamically
function loadStravaScript(callback) {
  const script = document.createElement('script');
  script.src = 'https://strava-embeds.com/embed.js';
  script.defer = true;
  script.onload = callback;
  document.head.appendChild(script);
}

// Handle initial route
document.addEventListener('DOMContentLoaded', () => {
  // Initial route handling
  handleRoute();

  // Load Strava script and initialize embeds
  loadStravaScript(() => {
    // Reinitialize Strava embeds when the carousel slides
    const carousel = document.getElementById('records-carousel');
    if (carousel) {
      carousel.addEventListener('slid.bs.carousel', () => {
        console.log('Carousel slide event triggered');
        initializeStravaEmbeds();
      });
    }

    // Initial load of Strava embeds
    initializeStravaEmbeds();
  });
});

// Handle route changes (back/forward buttons or manual URL entry)
window.addEventListener('popstate', () => handleRoute());

// Handle navigation link clicks
document.body.addEventListener('click', (event) => {
  if (event.target.tagName === 'A' && !event.target.classList.contains('dropdown-toggle')) {
    handleRoute(event);
  }
});
